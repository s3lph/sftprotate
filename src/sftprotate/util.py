# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from datetime import datetime, timedelta, timezone
import dateutil.parser
import re


def host_split(hoststr: str) -> dict:
    regex = re.compile('^((?P<protocol>[a-z0-9]+)://)?' +
                       '((?P<user>[a-zA-Z0-9_]+)@)?(?P<hostname>.*?)(#(?P<port>\d+))?:(?P<path>.+)$')
    match = regex.match(hoststr)
    if match is None:
        raise AttributeError('Invalid host: {}'.format(hoststr))
    hostconf = match.groupdict()
    if 'protocol' not in hostconf or hostconf['protocol'] is None:
        hostconf['protocol'] = 'sftp'
    return hostconf


def parse_timespan(tstr: str) -> timedelta:
    regex = re.compile('^((?P<w>\d+)w)?((?P<d>\d+)d)?((?P<h>\d+)h)?((?P<m>\d+)m)?((?P<s>\d+)s)?$')
    matches = regex.match(tstr)
    if matches is None:
        raise AttributeError('Invalid timespan string: {}'.format(tstr))
    if len(matches.group(0)) == 0:
        raise AttributeError('Invalid timespan string: {}'.format(tstr))
    tokens = matches.groupdict(0)
    return timedelta(
        weeks=int(tokens['w']),
        days=int(tokens['d']),
        hours=int(tokens['h']),
        minutes=int(tokens['m']),
        seconds=int(tokens['s'])
    )


def __to_utc(localtime: datetime) -> datetime:
    return localtime.astimezone(timezone.utc)


def extract_rfc3339(tstr: str) -> datetime:
    regex = re.compile('(\\b|(?<=_))\d{4}-\d{2}-\d{2}[Tt]\d{2}:\d{2}:\d{2}(.\d+)?([Zz]|([+\-]\d{2}:\d{2}))(\\b|(?=_))')
    match = regex.search(tstr)
    if match is None:
        raise ValueError('No RFC 3339 timestamp found in {}.'.format(tstr))
    datestring = match.group(0)
    localtime = dateutil.parser.parse(datestring)
    return __to_utc(localtime)
