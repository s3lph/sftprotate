# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from .protocol_abstraction import ProtocolAbstraction
from typing import List, Tuple
from datetime import datetime
import paramiko
import os
import stat


class SFTP(ProtocolAbstraction):
    """
    SFTP Wrapper implementation that uses paramiko.
    """

    @staticmethod
    def __load_user_ssh_config(input_config: dict) -> dict:
        # Create an empty SSH config object.
        config = paramiko.SSHConfig()
        # Open and parse the user's SSH config file, if it exists.
        config_path = os.path.expanduser('~/.ssh/config')
        if os.path.exists(config_path):
            with open(config_path) as file:
                config.parse(file)
        # Fetch the user's configuration for the provided hostname.
        return config.lookup(input_config['hostname'])

    def __setup_paramiko(self, input_config: dict):
        # Parse the user's SSH config file.
        ossh_config = self.__load_user_ssh_config(input_config)
        # Map OpenSSH config keys to paramiko config keys.
        para_config = {'key_filename': os.path.expanduser(ossh_config['identityfile'][0])}
        for osshkey, parakey in {'hostname': 'hostname',
                                 'user': 'username',
                                 'port': 'port',
                                 'identity_file': 'key_filename'}.items():
            if osshkey in ossh_config:
                para_config[parakey] = ossh_config[osshkey]
            # Override user config values with options passed to sftprotate.
            if osshkey is not 'hostname' and osshkey in input_config and input_config[osshkey] is not None:
                para_config[parakey] = input_config[osshkey]
        # Ensure the remote port is an int rather than a string
        if 'port' in para_config.keys():
            para_config['port'] = int(para_config['port'])

        # Initialize the SSH client.
        sshclient = paramiko.SSHClient()
        # Only establish connections to known hosts.
        sshclient.set_missing_host_key_policy(paramiko.RejectPolicy())
        # Load the user's known hosts and add them to the client's trusted keys.
        sshclient.load_system_host_keys()
        # Open a SSH connection.
        sshclient.connect(**para_config)
        self.__client = sshclient.open_sftp()
        self.__client.sshclient = sshclient

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__setup_paramiko(kwargs)

    @staticmethod
    def has_mtime() -> bool:
        return True

    @staticmethod
    def get_name() -> str:
        return 'SFTP'

    def make_directory(self, remote_dir: str):
        crumbs = []
        prev_head = remote_dir
        (head, tail) = os.path.split(remote_dir)
        while head is not None and not (head == '/' or len(head) == 0):
            crumbs.insert(0, prev_head)
            prev_head = head
            (head, tail) = os.path.split(head)
        crumbs.insert(0, prev_head)
        for path in crumbs:
            try:
                st = self.__client.stat(path)
                if not stat.S_ISDIR(st.st_mode):
                    raise NotADirectoryError(path)
            except FileNotFoundError:
                self.__client.mkdir(path)

    def put(self, local_file: str, remote_file: str):
        try:
            self.__client.stat(remote_file)
            raise FileExistsError('File exists on remote host: {}'.format(remote_file))
        except FileNotFoundError:
            self.__client.put(os.path.expanduser(local_file), remote_file)

    def list_files_and_dates(self, remote_dir: str) -> List[Tuple[str, datetime]]:
        # Fetch a list of paramiko.SFTPAttributes and translate it into wanted form.
        return [(f.filename, datetime.fromtimestamp(f.st_mtime)) for f in self.__client.listdir_attr(remote_dir)]

    def remove(self, remote_file: str):
        self.__client.remove(remote_file)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__client.close()
