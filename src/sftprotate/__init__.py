# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from sftprotate.meta import PACKAGE, VERSION
from sftprotate.sftprotate import SFTPRotate
from sftprotate.sftprotate_script import main
