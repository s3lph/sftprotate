# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

PACKAGE = 'sftprotate'
VERSION = '0.01'
