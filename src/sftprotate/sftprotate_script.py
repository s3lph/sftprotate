#!/usr/bin/env python3
#
# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

import sys
import os
from sys import stderr
from getopt import getopt, GetoptError
from datetime import timedelta
from functools import reduce

from sftprotate.util import parse_timespan, host_split
from sftprotate.meta import VERSION
from sftprotate.sftprotate import SFTPRotate


def show_help():
    protocol_list = reduce(lambda xs, x: xs + '\n    - {}'.format(x), SFTPRotate.get_protocols(), '')
    print("""
NAME
    sftprotate — A backup rotation tool that works with remote locations that restrict access to SFTP only.
    
SYNOPSIS

    sftprotate [options] local_src [proto://][user@]host[#port]:remote_dir

DESCRIPTION

    -h, -?, --help
        Display this help and exit.
        
    -v, --version
        Display version number and exit.
        
    -n, --maxnum=N
        Maximal number of backup instances to keep.  If there are more files in the remote directory than this value
        indicates, the oldest files are deleted, so that only as many files remain.  Won't delete files younger than
        minage.  Defaults to 3.
        
    -a, --minage=AGE
        Minimal age before a backup instance is deleted.  A file will not be deleted if it is younger than this value.
        Defaults to 7 days.  Required format:  [XXw][XXd][XXh][XXm][XXs]
        
    -t, --timestamp=METHOD
        Sets the method for determination of a remote file's age.  Possible values are 'mtime' and 'name' (default).
        When using 'mtime', a file's age is determined by the modification time reported by the remote server.  This may
        not be available with all protocols and on all systems.
        When using 'name', a file's name is expected to contain an RFC 3339 formatted timestamp somewhere in it,
        delimited by non-letters.  Use this if 'mtime' is not available or provides unrealiable information.
        Example:  backup-2018-01-30T13:37:42+01:00.tar.gz
        
    -i, --keyfile=KEYFILE
        Use KEYFILE as SSH private key file, overriding the IdentityFile entry in the user's ssh config file, if any. 
        
SUPPORTED PROTOCOLS{protocols}

AUTHOR
    Written by s3lph.

COPYRIGHT
    Copyright © 2018 s3lph.  License: MIT License <https://opensource.org/licenses/MIT>.
    
SEE ALSO
    sftp(1), stat(1), IETF RFC 3339 <https://tools.ietf.org/html/rfc3339>.
    """.format(protocols=protocol_list))


def parse_opts(argv: list) -> dict:
    opts = {
        'maxnum': 3,
        'minage': timedelta(days=7),
        'tsmethod': 'name',
        'keyfile_path': None
    }
    options = 'n:a:h?vt:i:'
    longoptions = ['maxnum', 'minage', 'help', 'help', 'version', 'timestamp', 'keyfile']
    try:
        optlist, args = getopt(argv, options, longoptions)
        for optopt, optarg in optlist:
            if optopt == '-h' or optopt == '-?' or optopt == '--help':
                show_help()
                exit(0)
            if optopt == '-v' or optopt == '--version':
                print("SFTPRotate {}".format(VERSION))
                exit(0)
        for optopt, optarg in optlist:
            if optopt == '-n' or optopt == '--maxnum':
                if len(optarg) == 0:
                    raise GetoptError('{} requires an argument'.format(optopt))
                opts['maxnum'] = int(optarg)
            if optopt == '-a' or optopt == '--minage':
                if len(optarg) == 0:
                    raise GetoptError('{} requires an argument'.format(optopt))
                opts['minage'] = parse_timespan(optarg)
            if optopt == '-t' or optopt == '--timestamp':
                if optarg != 'mtime' and optarg != 'name':
                    raise GetoptError('Unknown timestamp method: {}'.format(optarg))
                opts['tsmethod'] = optarg
            if optopt == '-i' or optopt == '--keyfile':
                if len(optopt) == 0:
                    raise GetoptError('{} requires an argument'.format(optopt))
                opts['keyfile_path'] = os.path.expanduser(optarg)

        if len(args) < 2:
            raise GetoptError('Too few arguments: src and dst required.')
        opts['localfile'] = args[0]
        __remote = host_split(args[1])
        opts['protocol'] = __remote['protocol']
        opts['remotedir'] = __remote['path']
        opts['protocol_config'] = {
            'hostname': __remote['hostname'],
            'user': __remote['user'],
            'port': __remote['port'],
            'identity_file': opts['keyfile_path']
        }
    except Exception as ex:
        print(ex, file=stderr)
        exit(1)
        pass
    return opts


def main():
    opts = parse_opts(sys.argv[1:])
    with SFTPRotate(opts['protocol_config'], opts['protocol'], maxnum=opts['maxnum'], minage=opts['minage'],
                    timestamp_method=opts['tsmethod']) as sftp:
        sftp.upload_backup_and_rotate(opts['localfile'], opts['remotedir'])


if __name__ == '__main__':
    main()
