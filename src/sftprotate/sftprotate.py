# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from typing import List, Tuple, Union
from datetime import timedelta, datetime, timezone
import os.path

from .protocol_abstraction import ProtocolAbstraction
from .sftp import SFTP
from .util import extract_rfc3339


class SFTPRotate:

    __PROTOCOLS = {
        'sftp': SFTP
    }

    def __init__(self,
                 protocol_config: dict,
                 protocol: Union[str, ProtocolAbstraction] = 'sftp',
                 maxnum: int = 3,
                 minage: timedelta = timedelta(days=7),
                 timestamp_method: str = 'mtime'):
        if isinstance(protocol, str):
            self.__protocol = SFTPRotate.__PROTOCOLS[protocol](**protocol_config)
        elif isinstance(protocol, ProtocolAbstraction):
            self.__protocol = protocol
        else:
            raise TypeError('protocol must be either a string or an instance of a ProtocolAbstraction subclass.')
        if timestamp_method == 'mtime' and not self.__protocol.has_mtime():
            raise AttributeError(
                '\'mtime\' timestamp method requested, but {} does not support it.'.format(self.__protocol.get_name()))
        self.__timespan_method = timestamp_method
        self.__maxnum = maxnum
        self.__minage = minage

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.__protocol.__exit__(exc_type, exc_value, exc_traceback)

    def __get_file_age(self, ref: datetime, file: Tuple[str, datetime]) -> timedelta:
        """
        Determines the age of a file relative to a reference date. Should be the same for all calls in a session.
        :param ref: The reference date.
        :param file: The file to get the age of.
        """
        parsed_date = None
        if self.__timespan_method == 'name':
            # Attempt to extract timestamp from filename if 'name' method was chosen.
            parsed_date = extract_rfc3339(file[0])
        elif self.__timespan_method == 'mtime':
            # Simply take the mtime fetched from the remote server if 'mtime' method was chosen.
            parsed_date = file[1]
        if not isinstance(parsed_date, datetime):
            raise TypeError('parsed_date is not a datetime object')
        if parsed_date.tzinfo is None:
            parsed_date = parsed_date.replace(tzinfo=timezone.utc)
        return ref - parsed_date

    def upload_backup(self, local_file: str, remote_dir: str):
        """
        Uploads a local file to a directory on the remote server. The directory will be created first, if needed.
        :param local_file: Path to the local file.
        :param remote_dir: Path to the directory on the remote server.
        """
        basename = os.path.basename(local_file)
        # Verify the filename contains an RFC3339 timestamp before uploading. This raises an exception, if not.
        if self.__timespan_method == 'time':
            extract_rfc3339(basename)
        self.__protocol.make_directory(remote_dir)
        remote_file = os.path.join(remote_dir, basename)
        self.__protocol.put(os.path.expanduser(local_file), remote_file)

    def rotate_directory(self, remote_dir: str):
        """
        Removes files in a remote directory that fulfil the requirements for deletion, i.e. number of files is over a
        given threshold and the file is older than a given age threshold.
        :param remote_dir: Path to the directory on the remote server.
        """
        # Reference date for file age computation
        now = datetime.now(timezone.utc)
        # Fetch the list of files in the directory and add the relative age to each tuple in the list.
        filelist = [(f[0], f[1], self.__get_file_age(now, f)) for f in self.__protocol.list_files_and_dates(remote_dir)]
        # Sort the list by relative age.
        filelist.sort(key=lambda f: f[2])
        # Build a list of filenames that are not in the first 'maxnum' and are older than 'minage'.
        to_remove = [x[0] for x in filelist[self.__maxnum:] if x[2] > self.__minage]
        # Remove each file that is present in the previously generated list.
        [self.__protocol.remove(os.path.join(remote_dir, f)) for f in to_remove]

    def upload_backup_and_rotate(self, local_file: str, remote_dir: str):
        """
        Uploads a local file to a directory on the remote server, and rotate backups in that directory. The directory
        will be created first, if needed.
        :param local_file: Path to the local file.
        :param remote_dir:  Path to the directory on the remote server.
        """
        self.upload_backup(local_file, remote_dir)
        self.rotate_directory(remote_dir)

    @staticmethod
    def get_protocols() -> List[str]:
        """
        Get the names of supported protocol abstractions.
        :returns A list of supported protocols.
        """
        return list(SFTPRotate.__PROTOCOLS.keys())
