# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from datetime import datetime
from typing import List, Tuple
from abc import ABC, abstractmethod


class ProtocolAbstraction(ABC):
    """
    This interface has to be implemented for each protocol that is to be supported.
    It provides methods to create directories, upload files, list files and modification time, and remove files.
    """

    @abstractmethod
    def __init__(self, **kwargs):
        """

        :param kwargs:
        """

    @staticmethod
    @abstractmethod
    def has_mtime() -> bool:
        """
        Whether a protocol supports modification time reporting.
        :returns A constant boolean value.
        """

    @staticmethod
    @abstractmethod
    def get_name() -> str:
        """
        :returns The name of the protocol.
        """

    @abstractmethod
    def make_directory(self, remote_dir: str):
        """
        Create a directory on the remote server. Any missing parent directories have to be created on-the-fly as well.
        :param remote_dir: Path of the remote directory to create, as entered by the user.
        """

    @abstractmethod
    def list_files_and_dates(self, remote_dir: str) -> List[Tuple[str, datetime]]:
        """
        List all regular files in a directory, and their modification time, if available. If modification time is not
        available, the returned mtime value should be set to None.
        :param remote_dir: The directory to list the contents of.
        :returns A list of (filename, mtime) tuples.
        """

    @abstractmethod
    def put(self, local_file: str, remote_file: str):
        """
        Upload a local file to the remote server.
        :param local_file: Path to the local file.
        :param remote_file: Path to the remote file.
        :raises OSError: If a file already exists.
        """

    @abstractmethod
    def remove(self, remote_file: str):
        """
        Delete a file from the remote server.
        :param remote_file: Path to the remote file.
        """

    @abstractmethod
    def __enter__(self):
        """
        Entry function for Python's contextmanager. Should return `self`.
        """

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Exit function for Python's contextmanager. Should close the underlying connection.
        :param exc_type: Exception type.
        :param exc_val: Exception value.
        :param exc_tb: Exception traceback.
        """
