# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from setuptools import setup
from sftprotate.meta import PACKAGE, VERSION

setup(
    name=PACKAGE,
    description='A backup rotation tool that works with remote locations that restrict access to sftp and scp only.',
    version=VERSION,
    entry_points={
        'console_scripts': [
            'sftprotate = sftprotate:main'
        ]
    },
    packages=[
        'sftprotate'
    ],
    install_requires=[
        'setuptools',
        'paramiko>=1.4',
        'python-dateutil'
    ],
    author="s3lph",
    license="MIT"
)
