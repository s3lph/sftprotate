# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from unittest import TestCase
from sftprotate.util import parse_timespan


class TestParseTimeSpan(TestCase):

    def test_seconds(self):
        ts = parse_timespan('70s')
        self.assertEqual(70, ts.seconds)
        self.assertEqual(0, ts.microseconds)
        self.assertEqual(0, ts.days)

    def test_minutes(self):
        ts = parse_timespan('61m')
        self.assertEqual(61*60, ts.seconds)
        self.assertEqual(0, ts.microseconds)
        self.assertEqual(0, ts.days)

    def test_hours(self):
        ts = parse_timespan('1h')
        self.assertEqual(3600, ts.seconds)
        self.assertEqual(0, ts.microseconds)
        self.assertEqual(0, ts.days)

    def test_hours_2(self):
        ts = parse_timespan('25h')
        self.assertEqual(3600, ts.seconds)
        self.assertEqual(0, ts.microseconds)
        self.assertEqual(1, ts.days)

    def test_weeks(self):
        ts = parse_timespan('2w')
        self.assertEqual(0, ts.seconds)
        self.assertEqual(0, ts.microseconds)
        self.assertEqual(14, ts.days)

    def test_all(self):
        ts = parse_timespan('1w1d1h1m1s')
        self.assertEqual(3661, ts.seconds)
        self.assertEqual(0, ts.microseconds)
        self.assertEqual(8, ts.days)

    def test_empty(self):
        try:
            parse_timespan('')
            self.fail()
        except AttributeError:
            return

    def test_num_char_swap(self):
        try:
            parse_timespan('s42')
            self.fail()
        except AttributeError:
            return

    def test_invalid_part(self):
        try:
            parse_timespan('10m20x42s')
            self.fail()
        except AttributeError:
            return
