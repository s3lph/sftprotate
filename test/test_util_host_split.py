# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from unittest import TestCase
from sftprotate.util import host_split


class TestHostSplit(TestCase):

    def test_all_args_set(self):
        hostdict = host_split('sftp://root@foo.bar#1234:/etc/passwd')
        self.assertEqual('sftp', hostdict['protocol'])
        self.assertEqual('foo.bar', hostdict['hostname'])
        self.assertEqual('1234', hostdict['port'])
        self.assertEqual('root', hostdict['user'])
        self.assertEqual('/etc/passwd', hostdict['path'])

    def test_empty_string(self):
        try:
            host_split('')
            self.fail()
        except AttributeError:
            return

    def test_host_path_only(self):
        hostdict = host_split('foo:/etc/passwd')
        self.assertEqual('sftp', hostdict['protocol'])
        self.assertEqual('foo', hostdict['hostname'])
        self.assertEqual(None, hostdict['port'])
        self.assertEqual(None, hostdict['user'])
        self.assertEqual('/etc/passwd', hostdict['path'])
