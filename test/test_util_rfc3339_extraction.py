# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from datetime import timedelta
from unittest import TestCase
from sftprotate.util import extract_rfc3339


class TestRFC3339Extraction(TestCase):

    def test_rfc_examples_timestamp_only(self):
        date = extract_rfc3339('1985-04-12T23:20:50.52Z')
        self.assertEqual(1985, date.year)
        self.assertEqual(4, date.month)
        self.assertEqual(12, date.day)
        self.assertEqual(23, date.hour)
        self.assertEqual(20, date.minute)
        self.assertEqual(50, date.second)
        self.assertEqual(520000, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

        date = extract_rfc3339('1996-12-19T16:39:57-08:00')
        self.assertEqual(1996, date.year)
        self.assertEqual(12, date.month)
        self.assertEqual(20, date.day)
        self.assertEqual(0, date.hour)
        self.assertEqual(39, date.minute)
        self.assertEqual(57, date.second)
        self.assertEqual(0, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

        date = extract_rfc3339('1937-01-01t12:00:27.87+00:20')
        self.assertEqual(1937, date.year)
        self.assertEqual(1, date.month)
        self.assertEqual(1, date.day)
        self.assertEqual(11, date.hour)
        self.assertEqual(40, date.minute)
        self.assertEqual(27, date.second)
        self.assertEqual(870000, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

    def test_rfc_examples_surrounded_correct(self):
        date = extract_rfc3339('1985-04-12T23:20:50.52Z-backup.tar.gz')
        self.assertEqual(1985, date.year)
        self.assertEqual(4, date.month)
        self.assertEqual(12, date.day)
        self.assertEqual(23, date.hour)
        self.assertEqual(20, date.minute)
        self.assertEqual(50, date.second)
        self.assertEqual(520000, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

        # Who on earth would do this? Still gonna test it, though...
        date = extract_rfc3339('backup.tar.gz-1996-12-19T16:39:57-08:00')
        self.assertEqual(1996, date.year)
        self.assertEqual(12, date.month)
        self.assertEqual(20, date.day)
        self.assertEqual(0, date.hour)
        self.assertEqual(39, date.minute)
        self.assertEqual(57, date.second)
        self.assertEqual(0, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

        date = extract_rfc3339('backup_1937-01-01T12:00:27.87+00:20-xyz.tar.gz')
        self.assertEqual(1937, date.year)
        self.assertEqual(1, date.month)
        self.assertEqual(1, date.day)
        self.assertEqual(11, date.hour)
        self.assertEqual(40, date.minute)
        self.assertEqual(27, date.second)
        self.assertEqual(870000, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

        date = extract_rfc3339('backup_1937-01-01t12:00:27.87+00:20_xyz.tar.gz')
        self.assertEqual(1937, date.year)
        self.assertEqual(1, date.month)
        self.assertEqual(1, date.day)
        self.assertEqual(11, date.hour)
        self.assertEqual(40, date.minute)
        self.assertEqual(27, date.second)
        self.assertEqual(870000, date.microsecond)
        self.assertEqual(timedelta(0), date.utcoffset())

    def test_surrounded_fail(self):
        try:
            extract_rfc3339('backup.tar.gz')
        except ValueError:
            pass
        try:
            extract_rfc3339('backup1937-01-01T12:00:27.87+00:20_xyz.tar.gz')
            self.fail()
        except ValueError:
            pass
        try:
            extract_rfc3339('backup-1937-01-01T12:00:27.87zxyz.tar.gz')
            self.fail()
        except ValueError:
            pass

    def test_invalid_timestamp_only(self):
        try:
            extract_rfc3339('1937-01-01T12:00:27.87+00')
            self.fail()
        except ValueError:
            pass
        try:
            extract_rfc3339('1937-01-01T12:00:27.+00:20')
            self.fail()
        except ValueError:
            pass
        try:
            extract_rfc3339('1937-01-01x12:00:27.87+00:20')
            self.fail()
        except ValueError:
            pass
        try:
            extract_rfc3339('1984-01-01T42:00:00.00+00:00')
            self.fail()
        except ValueError:
            pass
