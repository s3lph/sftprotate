# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from unittest import TestCase
import os
import pwd
import stat

from sftprotate.sftp import SFTP


class TestSFTP(TestCase):

    def setUp(self):
        # UID and GID needed to prepare file ownerships for test cases
        (self.__sftpuser_uid, self.__sftpuser_gid) = pwd.getpwnam('sftprotate')[2:4]

    def assert_mkdir_result(self, p: str):
        self.assertTrue(os.path.isdir(p))
        st = os.stat(p)
        self.assertEqual(stat.S_IRWXU, st.st_mode & stat.S_IRWXU)
        self.assertEqual(st.st_uid, self.__sftpuser_uid)
        self.assertEqual(st.st_gid, self.__sftpuser_gid)

    def test_sftp_name(self):
        """
        get_name should always return 'SFTP'. Primarily here for coverage reasons
        """
        with SFTP(**{'hostname': 'localhost'}) as sftp:
            self.assertEqual('SFTP', sftp.get_name())

    def test_sftp_mkdir_noexist(self):
        """
        make_directory should be able to create a directory, assuming permissions are correct.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        with SFTP(**{'hostname': 'localhost'}) as sftp:
            sftp.make_directory('/var/sftprotate_test/sftp/backup')
        self.assert_mkdir_result('/var/sftprotate_test/sftp/backup')

    def test_sftp_mkdir_exist(self):
        """
        make_directory must not throw if the directory already exists and has sufficient permissions.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        os.mkdir('/var/sftprotate_test/sftp/backup')
        os.chown('/var/sftprotate_test/sftp/backup', self.__sftpuser_uid, self.__sftpuser_gid)
        os.chmod('/var/sftprotate_test/sftp/backup', stat.S_IWUSR | stat.S_IREAD | stat.S_IEXEC)
        with SFTP(**{'hostname': 'localhost'}) as sftp:
            sftp.make_directory('/var/sftprotate_test/sftp/backup')
        self.assert_mkdir_result('/var/sftprotate_test/sftp/backup')

    def test_sftp_mkdir_multiple(self):
        """
        make_directory should be able to create multiple levels of directories, assuming permissions are correct.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        with SFTP(**{'hostname': 'localhost'}) as sftp:
            sftp.make_directory('/var/sftprotate_test/sftp/backup/my_awesome_service/staging')
        self.assert_mkdir_result('/var/sftprotate_test/sftp/backup/my_awesome_service/staging')

    def test_sftp_mkdir_eacces(self):
        """
        make_directory must throw if not enough permissions to mkdir.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        os.mkdir('/var/sftprotate_test/sftp/backup')
        os.chown('/var/sftprotate_test/sftp/backup', 0, 0)
        os.chmod('/var/sftprotate_test/sftp/backup', stat.S_IWUSR | stat.S_IREAD | stat.S_IEXEC)
        with self.assertRaises(PermissionError):
            with SFTP(**{'hostname': 'localhost'}) as sftp:
                sftp.make_directory('/var/sftprotate_test/sftp/backup/my_awesome_service')

    def test_sftp_mkdir_broken_symlink(self):
        """
        make_directory must throw if dir is a broken symlink.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        os.symlink('/nonexistent', '/var/sftprotate_test/sftp/backup', target_is_directory=True)
        os.lchown('/var/sftprotate_test/sftp/backup', self.__sftpuser_uid, self.__sftpuser_gid)
        with self.assertRaises(IOError):
            with SFTP(**{'hostname': 'localhost'}) as sftp:
                sftp.make_directory('/var/sftprotate_test/sftp/backup/my_awesome_service')

    def test_sftp_file_upload_exists(self):
        """
        file upload must throw if file exists.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        os.mkdir('/var/sftprotate_test/sftp/backup')
        os.chown('/var/sftprotate_test/sftp/backup', self.__sftpuser_uid, self.__sftpuser_gid)
        os.chmod('/var/sftprotate_test/sftp/backup', stat.S_IWUSR | stat.S_IREAD | stat.S_IEXEC)
        dfile = open('/var/sftprotate_test/sftp/backup/testfile', 'w')
        dfile.write('this must not be touched')
        dfile.close()
        sfile = open('testfile', 'w')
        sfile.write('file was touched')
        sfile.close()
        with self.assertRaises(OSError):
            with SFTP(**{'hostname': 'localhost'}) as sftp:
                sftp.put('testfile', '/var/sftprotate_test/sftp/backup/testfile')
        dfile = open('/var/sftprotate_test/sftp/backup/testfile', 'r')
        self.assertEqual('this must not be touched', dfile.readline())
        dfile.close()

    def test_sftp_file_upload(self):
        """
        file upload must throw if file exists.
        """
        # Clean up
        try:
            os.system('rm -rf /var/sftprotate_test/sftp/backup')
        except FileNotFoundError:
            pass
        os.mkdir('/var/sftprotate_test/sftp/backup')
        os.chown('/var/sftprotate_test/sftp/backup', self.__sftpuser_uid, self.__sftpuser_gid)
        os.chmod('/var/sftprotate_test/sftp/backup', stat.S_IWUSR | stat.S_IREAD | stat.S_IEXEC)
        sfile = open('testfile', 'w')
        sfile.write('my file')
        sfile.close()
        with SFTP(**{'hostname': 'localhost'}) as sftp:
            sftp.put('testfile', '/var/sftprotate_test/sftp/backup/testfile')
        dfile = open('/var/sftprotate_test/sftp/backup/testfile', 'r')
        self.assertEqual('my file', dfile.readline())
        dfile.close()
