# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from typing import List, Tuple
from datetime import datetime, timedelta, timezone
from unittest import TestCase
from unittest.mock import MagicMock, call
import os.path

from sftprotate import SFTPRotate
from sftprotate.protocol_abstraction import ProtocolAbstraction


class MockSFTP(ProtocolAbstraction):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__mock_list = []
        self.put = MagicMock(side_effect=self.__put)
        self.list_files_and_dates = MagicMock(side_effect=self.__list_files_and_dates,
                                              return_value=self.__mock_list)
        self.remove = MagicMock(side_effect=self.__remove)
        self.make_directory = MagicMock(side_effect=self.__make_directory)
        self.has_mtime = MagicMock(return_value=kwargs.get('has_mtime', True))

    @staticmethod
    def has_mtime() -> bool:
        return True

    @staticmethod
    def get_name() -> str:
        return 'MockSFTP'

    def __put(self, _: str, remote_file: str):
        filename = os.path.split(remote_file)[-1]
        if filename in [f[0] for f in self.__mock_list]:
            raise OSError('File already exists')
        self.__mock_list.append((filename, datetime.utcnow()))

    def __list_files_and_dates(self, _: str) -> List[Tuple[str, datetime]]:
        return self.__mock_list

    def __remove(self, remote_file: str):
        file = os.path.split(remote_file)[-1]
        self.__mock_list = [f for f in self.__mock_list if f[0] is not file]

    def __make_directory(self, remote_dir: str):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def put(self, local_file: str, remote_file: str):
        pass

    def list_files_and_dates(self, remote_dir: str) -> List[Tuple[str, datetime]]:
        pass

    def remove(self, remote_file: str):
        pass

    def make_directory(self, remote_dir: str):
        pass


class TestSFTPRotate(TestCase):

    def test_protocol_list(self):
        """
        Make sure the default protocol is included in the protocol list.
        """
        protocols = SFTPRotate.get_protocols()
        self.assertIn('sftp', protocols)
        try:
            with SFTPRotate({}, 'sftp'):
                pass
        except KeyError as e:
            # This will throw an error either way, because no configuration was provided for paramiko.
            if e.__str__() != '\'hostname\'' and e.__str__() != '\'username\'' and e.__str__() != '\'port\'':
                self.fail()

    def test_invalid_protocol(self):
        """
        A KeyError must be raised if a invalid protocol name is provided.
        A TypeError must be raised if a non-string, non-ProtocolAbstraction is provided.
        """
        try:
            SFTPRotate({}, 'htcpcp')
            self.fail()
        except KeyError:
            pass
        try:
            # This test is mostly about coverage.
            # noinspection PyTypeChecker
            SFTPRotate({}, 42)
            self.fail()
        except TypeError:
            pass

    def test_default_arguments(self):
        """
        Make sure the default parameters are as intended.
        """
        mock = MockSFTP()
        with SFTPRotate({}, mock) as sftp:
            self.assertEqual('mtime', sftp._SFTPRotate__timespan_method)
            self.assertEqual(3, sftp._SFTPRotate__maxnum)
            self.assertEqual(timedelta(7), sftp._SFTPRotate__minage)

    def test_no_mtime_timestamp_mtime_requested(self):
        """
        If a protocol does not support modification time, but 'mtime' method was requested, an AttributeError must be
        raised.
        """
        try:
            mock = MockSFTP(**{'has_mtime': False})
            SFTPRotate({}, mock, timestamp_method='mtime')
            self.fail()
        except AttributeError:
            pass

    def test_no_mtime_timestamp_name_requested(self):
        """
        If a protocol does not support modification time, and 'name' method was requested, no error must be raised.
        """
        mock = MockSFTP(**{'has_mtime': False})
        with SFTPRotate({}, mock, timestamp_method='name'):
            pass

    def test_mtime_age(self):
        """
        Test file age determination with 'mtime' method.
        """
        testmtime = datetime(year=2018, month=1, day=27, hour=0, minute=0, second=0, tzinfo=timezone.utc)
        testnow = datetime(year=2018, month=2, day=3, hour=13, minute=37, second=42, tzinfo=timezone.utc)
        mock = MockSFTP()
        with SFTPRotate({}, mock, timestamp_method='mtime') as sftp:
            self.assertEqual(timedelta(weeks=1, hours=13, minutes=37, seconds=42),
                             sftp._SFTPRotate__get_file_age(testnow, ('foo', testmtime)))

    def test_name_age(self):
        """
        Test file age determination with 'name' method.
        """
        testnow = datetime(year=2018, month=2, day=3, hour=13, minute=37, second=42,
                           tzinfo=timezone.utc)
        mock = MockSFTP()
        with SFTPRotate({}, mock, timestamp_method='name') as sftp:
            self.assertEqual(timedelta(weeks=1, hours=13, minutes=37, seconds=42),
                             sftp._SFTPRotate__get_file_age(
                                 testnow, ('foo-2018-01-27T00:00:00+00:00.tar.gz', None)))

    def test_empty_dir(self):
        """
        Test file upload to a nonexistent/empty directory. No file deletion must occur.
        """
        mock = MockSFTP()
        with SFTPRotate({}, mock) as sftp:
            sftp.upload_backup_and_rotate('foo', '/bar/baz')
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_called_once_with('/bar/baz')
            mock.remove.assert_not_called()

    def test_put_file_exists(self):
        """
        Test file upload to a destination file that already exists. An OSError must be raised.
        """
        mock = MockSFTP()
        mock._MockSFTP__mock_list.append(('foo', datetime.now(timezone.utc)))
        with SFTPRotate({}, mock) as sftp:
            try:
                sftp.upload_backup_and_rotate('foo', '/bar/baz')
                self.fail('upload_backup_and_rotate should raise an OSError if a file already exists.')
            except OSError:
                pass
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_not_called()
            mock.remove.assert_not_called()

    def test_put_no_delete_default_config(self):
        """
        Test file upload with one other file already there. No file deletion must occur.
        """
        mock = MockSFTP()
        mock._MockSFTP__mock_list.append(('qux', datetime.now(timezone.utc)))
        with SFTPRotate({}, mock) as sftp:
            sftp.upload_backup_and_rotate('foo', '/bar/baz')
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_called_once_with('/bar/baz')
            mock.remove.assert_not_called()

    def test_delete_oldest_one_file_default_config(self):
        """
        Test file upload with three old files already there. The oldest file must be deleted.
        """
        mock = MockSFTP()
        mock._MockSFTP__mock_list.append(('file1', datetime(year=1990, month=1, day=1)))
        mock._MockSFTP__mock_list.append(('file2', datetime(year=1984, month=4, day=4)))
        mock._MockSFTP__mock_list.append(('file3', datetime(year=2000, month=1, day=1)))
        with SFTPRotate({}, mock) as sftp:
            sftp.upload_backup_and_rotate('foo', '/bar/baz')
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_called_once_with('/bar/baz')
            mock.remove.assert_called_once_with('/bar/baz/file2')

    def test_no_delete_oldest_one_file_not_old_enough_default_config(self):
        """
        Test file upload with three old, but not old enough, files already there. No file must be deleted.
        """
        refdate = datetime.utcnow()
        mock = MockSFTP()
        mock._MockSFTP__mock_list.append(('file1', refdate - timedelta(days=1)))
        mock._MockSFTP__mock_list.append(('file2', refdate - timedelta(days=2)))
        mock._MockSFTP__mock_list.append(('file3', refdate - timedelta(days=3)))
        with SFTPRotate({}, mock) as sftp:
            sftp.upload_backup_and_rotate('foo', '/bar/baz')
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_called_once_with('/bar/baz')
            mock.remove.assert_not_called()

    def test_delete_oldest_one_file_reduced_minage(self):
        """
        Test file upload with three old files already there, but with reduced minimum file age. No file must be deleted.
        """
        refdate = datetime.now(timezone.utc)
        mock = MockSFTP()
        mock._MockSFTP__mock_list.append(('file1', refdate - timedelta(days=1)))
        mock._MockSFTP__mock_list.append(('file2', refdate - timedelta(days=2)))
        mock._MockSFTP__mock_list.append(('file3', refdate - timedelta(days=3)))
        with SFTPRotate({}, mock, minage=timedelta(days=1)) as sftp:
            sftp.upload_backup_and_rotate('foo', '/bar/baz')
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_called_once_with('/bar/baz')
            mock.remove.assert_called_once_with('/bar/baz/file3')

    def test_delete_oldest_two_files_reduced_minage_reduced_maxnum(self):
        """
        Test file upload with three old files already there, but with reduced minimum file age and reduced maximum file
        number. The two oldest files must be deleted.
        """
        refdate = datetime.now(timezone.utc)
        mock = MockSFTP()
        mock._MockSFTP__mock_list.append(('file1', refdate - timedelta(days=1)))
        mock._MockSFTP__mock_list.append(('file2', refdate - timedelta(days=2)))
        mock._MockSFTP__mock_list.append(('file3', refdate - timedelta(days=3)))
        with SFTPRotate({}, mock, minage=timedelta(days=1), maxnum=2) as sftp:
            sftp.upload_backup_and_rotate('foo', '/bar/baz')
            mock.make_directory.assert_called_once_with('/bar/baz')
            mock.put.assert_called_once_with('foo', '/bar/baz/foo')
            mock.list_files_and_dates.assert_called_once_with('/bar/baz')
            mock.remove.assert_has_calls([call('/bar/baz/file2'), call('/bar/baz/file3')], any_order=True)
            try:
                mock.remove.assert_called_with('/bar/baz/file1')
                self.fail()
            except AssertionError:
                pass
            try:
                mock.remove.assert_called_with('/bar/baz/foo')
                self.fail()
            except AssertionError:
                pass
