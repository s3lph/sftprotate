# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

from unittest import TestCase
from datetime import timedelta

from sftprotate.sftprotate_script import parse_opts


class TestSFTPRotateScript(TestCase):

    def test_getopt_names_only(self):
        opts = parse_opts(['localfile42', 'remotehost:remotedir1337'])
        self.assertEqual(3, opts['maxnum'])
        self.assertEqual(timedelta(days=7), opts['minage'])
        self.assertEqual('sftp', opts['protocol'])
        self.assertEqual('localfile42', opts['localfile'])
        self.assertEqual('remotedir1337', opts['remotedir'])
        self.assertEqual('remotehost', opts['protocol_config']['hostname'])
        self.assertEqual(None, opts['protocol_config']['user'])
        self.assertEqual(None, opts['protocol_config']['port'])
        self.assertEqual(None, opts['protocol_config']['identity_file'])
        self.assertEqual('name', opts['tsmethod'])
        self.assertEqual(None, opts['keyfile_path'])

