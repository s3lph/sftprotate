# This file is part of SFTPRotate
# Copyright © 2018 s3lph
# Distributed under the MIT License <https://opensource.org/licenses/MIT>

# noinspection PyUnresolvedReferences
from test_util_host_split import *
# noinspection PyUnresolvedReferences
from test_util_parse_timespan import *
# noinspection PyUnresolvedReferences
from test_util_rfc3339_extraction import *
# noinspection PyUnresolvedReferences
from test_sftprotate import *
# noinspection PyUnresolvedReferences
from test_sftp import *
# noinspection PyUnresolvedReferences
from test_sftprotate_script import *

import unittest.main

if __name__ == '__main__':
    unittest.main()
